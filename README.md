### TAP Install Scripts
- The title is pretty self explanatory

1. `mv varfiles/tanzu-install-vars-dockerhub-norelocate.env.REDACTED varfiles/tanzu-install-vars-dockerhub-norelocate.env`
2. edit the vars file with your Dockerhub and Tanzunet details
3. `./install-tap11.sh ./varfiles/tanzu-install-vars-dockerhub-norelocate.env`
4.  If your VM is running on 192.168.5.20, point a browser to `http://tap-gui.192.168.5.20.sslip.io:8080`
