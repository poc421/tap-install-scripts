#!/usr/bin/env bash


set -o errexit
set -o nounset
set -o pipefail
set -o xtrace
set -x

WORKDIR=$(dirname "${BASH_SOURCE[0]}") 
cd $WORKDIR

mkdir -p $HOME/bin
mkdir -p $WORKDIR/tanzu
mkdir -p $WORKDIR/workaround
mkdir -p $WORKDIR/cluster_essentials
mkdir -p $WORKDIR/tap11
source ~/.profile

echog() {
    echo -e "\e[00;32m$1\e[00m"
}
echor() {
    echo -e "\e[00;31m$1\e[00m"
}



if [[ $# -ne 1 ]]; then
    echor "Usage: install-tap11.sh  <vars-file.env>"
    exit 1
else
    VAR_FILE=$(realpath $1)
fi




check_pivnet() {
    echog "downloading and installing tanzu cli and plugins"
    # download pivnet cli and smoke test
    if [ -f $HOME/bin/pivnet ]; then
    echog "found pivnet"
    else
    echor "don't see pivnet in local bin, downloading"
        curl $PIVNET_CLI_LOC -L -o $HOME/bin/pivnet
        chmod +x $HOME/bin/pivnet
    fi
    $HOME/bin/pivnet version

    # login to tanzunet
    if [[ -z "${TANZUNET_APITOKEN-}" ]]; then
        read -p "enter TANZUNET_APITOKEN:  " TANZUNET_APITOKEN
    else
        echog  "found env var TANZUNET_APITOKEN"
    fi

    pivnet login --api-token $TANZUNET_APITOKEN

}

download_and_install_tanzu_cli_and_plugins(){
    
    check_pivnet

    rettxt=$(eval $TAP_PIVNET_DOWNLOAD_CMD)
    # echo "$rettxt"
    tar -xvf "tanzu-framework-linux-amd64.tar" -C $WORKDIR/tanzu

    install "$WORKDIR/tanzu/cli/core/v0.11.2/tanzu-core-linux_amd64" $HOME/bin/tanzu
    tanzu version

    tanzu plugin install --local $WORKDIR/tanzu/cli all
    tanzu plugin list

    rm tanzu-framework-linux-amd64.tar

}

patch_plugins(){
    curl -L -o $WORKDIR/workaround/tanzu-workaround.tar.gz $PLUGIN_WORKAROUND_RELEASE_URL
    tar xvfz $WORKDIR/workaround/tanzu-workaround.tar.gz -C $WORKDIR/workaround
    ls -al $WORKDIR/workaround
    tanzu plugin install --local $WORKDIR/workaround/standalone-plugins package
    tanzu plugin install --local $WORKDIR/workaround/standalone-plugins secret
    tanzu plugin list
    tanzu secret version
    tanzu package version
}

install_unmanaged_cluster(){
    mkdir -p $WORKDIR/tce

    if tanzu uc version; then
        echog "we already have the unmanaged-cluster plugin"
    else
        echog "installing the unmanaged-cluster plugin"
        curl -L -o $WORKDIR/tce/tce.tar.gz $TCE_RELEASE_URL
        tar xvfz $WORKDIR/tce/tce.tar.gz -C $WORKDIR/tce
        tanzu plugin install --local $WORKDIR/tce/tce-linux-amd64-v0.11.0/default-local unmanaged-cluster
    fi

    if tanzu uc list | grep my-unmanaged-cluster; then
        echog "unmanaged cluster already exists"
    else
        echog "creating unmanaged cluster"
        tanzu uc create my-unmanaged-cluster -p 80:8080 -p 443:8443
    fi

    if kubectl version &> /dev/null; then
        echog "we appear to have a kube cluster"
    else
        echor "still no k8s cluster!!"
        exit 1
    fi


}

install_cluster_essentials() {
    check_pivnet
    rettxt=$(eval $CLUSTER_ESSENTIALS_PIVNET_DOWNLOAD_CMD)
    mv tanzu-cluster-essentials-linux-amd64-1.1.0.tgz $WORKDIR/cluster_essentials
    tar xvfz $WORKDIR/cluster_essentials/tanzu-cluster-essentials-linux-amd64-1.1.0.tgz -C $WORKDIR/cluster_essentials
    pushd $WORKDIR/cluster_essentials
    INSTALL_BUNDLE=$CLUSTER_ESSENTIALS_INSTALL_BUNDLE
    INSTALL_REGISTRY_HOSTNAME=$CLUSTER_ESSENTIALS_INSTALL_REGISTRY_HOSTNAME
    INSTALL_REGISTRY_USERNAME=$CLUSTER_ESSENTIALS_INSTALL_REGISTRY_USERNAME
    INSTALL_REGISTRY_PASSWORD=$CLUSTER_ESSENTIALS_INSTALL_REGISTRY_PASSWORD

    ns_name=tanzu-cluster-essentials
    echog "## Creating namespace $ns_name"
cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: Namespace
metadata:
    name: ${ns_name}
EOF

    echog "## Pulling bundle from $INSTALL_REGISTRY_HOSTNAME (username: $INSTALL_REGISTRY_USERNAME)"

    [ -z "$INSTALL_BUNDLE" ]            && { echo "INSTALL_BUNDLE env var must not be empty"; exit 1; }
    [ -z "$INSTALL_REGISTRY_HOSTNAME" ] && { echo "INSTALL_REGISTRY_HOSTNAME env var must not be empty"; exit 1; }
    [ -z "$INSTALL_REGISTRY_USERNAME" ] && { echo "INSTALL_REGISTRY_USERNAME env var must not be empty"; exit 1; }
    [ -z "$INSTALL_REGISTRY_PASSWORD" ] && { echo "INSTALL_REGISTRY_PASSWORD env var must not be empty"; exit 1; }

    export IMGPKG_REGISTRY_HOSTNAME_0=$INSTALL_REGISTRY_HOSTNAME
    export IMGPKG_REGISTRY_USERNAME_0=$INSTALL_REGISTRY_USERNAME
    export IMGPKG_REGISTRY_PASSWORD_0=$INSTALL_REGISTRY_PASSWORD
    ./imgpkg pull -b $INSTALL_BUNDLE -o ./bundle/

    export YTT_registry__server=$INSTALL_REGISTRY_HOSTNAME
    export YTT_registry__username=$INSTALL_REGISTRY_USERNAME
    export YTT_registry__password=$INSTALL_REGISTRY_PASSWORD


    if kubectl get app &> /dev/null; then
        echog "kapp-controller appears to be already installed"
    else
        echog "installing kapp-controller"
        ./kapp deploy --yes -a kapp-controller -n $ns_name -f <(./ytt -f ./bundle/kapp-controller/config/ -f ./bundle/registry-creds/ --data-values-env YTT --data-value-yaml kappController.deployment.concurrency=10 | ./kbld -f- -f ./bundle/.imgpkg/images.yml) "$@"
    fi

    if kubectl get secretexport &> /dev/null; then
        echog "secretgen-controller appears to be already installed"
    else
        echog "installing secretgen-controller"
        ./kapp deploy --yes -a secretgen-controller -n $ns_name -f <(./ytt -f ./bundle/secretgen-controller/config/ -f ./bundle/registry-creds-schema/ -f ./bundle/registry-creds/ --data-values-env YTT | ./kbld -f- -f ./bundle/.imgpkg/images.yml) "$@"
    fi
    popd
}  

install_kubectl() {
    curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
    chmod +x kubectl 
    mv kubectl $HOME/bin
}


install_tap() {

    echog "Installing TAP"

    kubectl create ns tap-install --dry-run=client -oyaml | kubectl apply -f-

    if [[ $RELOCATE_TAP_IMAGES == "true" ]]; then
        docker login $TANZUNET_REGISTRY_HOSTNAME -u $TANZUNET_REGISTRY_USERNAME -p $TANZUNET_REGISTRY_PASSWORD
        #docker: repository-1.docker.io
        #harbor: harbor.poc1.k10s.io
        docker login $RELOCATE_DOCKER_LOGIN_SERVER -u $RELOCATE_REGISTRY_USERNAME -p $RELOCATE_REGISTRY_PASSWORD

        ${WORKDIR}/cluster_essentials/imgpkg copy -b ${TANZUNET_REGISTRY_HOSTNAME}/tanzu-application-platform/tap-packages:${TAP_VERSION} \
                    --to-repo ${RELOCATED_IMAGES_REPO}        
    fi
    
    # if [[ -z $TAP_REPO ]]; then
    #     echog "tap repo is empty"
    #     TAP_REPO="/"
    # else
    #     echog "adding slashes to TAP_REPO"
    #     TAP_REPO=/${TAP_REPO}/
    #     echog "new TAP_REPO: ${TAP_REPO}"
    # fi
    echog "adding tap package repo to the cluster"
    
    #repo secret - for downloading tap packages
    # docker: https://repository-1.docker.io/
    # harbor: harbor.poc1.k10s.io
    # tanzu: registry.tanzu.vmware.com
    tanzu secret registry add tap-registry \
        --username ${TAP_REG_SECRET_USERNAME} --password ${TAP_REG_SECRET_PASSWORD} \
        --server ${TAP_REG_SERVER} \
        --export-to-all-namespaces --yes --namespace tap-install



    tanzu package repository add tanzu-tap-repository \
        --url ${RELOCATED_IMAGES_REPO}:${TAP_VERSION} \
        --namespace tap-install

    tanzu package repository get tanzu-tap-repository --namespace tap-install

    tanzu package available list --namespace tap-install

        if [[ ! -f ${WORKDIR}/tap-values-template.yaml ]]; then
        echor "Could not find tap-values-template.yaml"
        exit 1
    fi

    cat ${WORKDIR}/tap-values-template.yaml | envsubst > ${WORKDIR}/tap11/tap-values.yaml

    echog "INSTALLING THE TAP PACKAGE WITH VALUES FROM ${WORKDIR}/tap11/tap-values.yaml"

    tanzu package install tap -p tap.tanzu.vmware.com -v $TAP_VERSION --values-file ${WORKDIR}/tap11/tap-values.yaml -n tap-install


}  

install_dev_ns() {
        echog "creating dev ns at ${DEV_NS}"
    kubectl create ns $DEV_NS --dry-run=client -oyaml | kubectl apply -f-

    tanzu secret registry add registry-credentials --server ${DEV_REG_SERVER_SECRET} \
        --username ${DEV_REG_SECRET_USERNAME} --password ${DEV_REG_SECRET_PASSWORD} \
        --namespace ${DEV_NS}

cat <<EOF | kubectl -n ${DEV_NS} apply -f -
apiVersion: v1
kind: Secret
metadata:
  name: tap-registry
  annotations:
    secretgen.carvel.dev/image-pull-secret: ""
type: kubernetes.io/dockerconfigjson
data:
  .dockerconfigjson: e30K
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: default
secrets:
  - name: registry-credentials
imagePullSecrets:
  - name: registry-credentials
  - name: tap-registry
---
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: default
rules:
- apiGroups: [source.toolkit.fluxcd.io]
  resources: [gitrepositories]
  verbs: ['*']
- apiGroups: [source.apps.tanzu.vmware.com]
  resources: [imagerepositories]
  verbs: ['*']
- apiGroups: [carto.run]
  resources: [deliverables, runnables]
  verbs: ['*']
- apiGroups: [kpack.io]
  resources: [images]
  verbs: ['*']
- apiGroups: [conventions.apps.tanzu.vmware.com]
  resources: [podintents]
  verbs: ['*']
- apiGroups: [""]
  resources: ['configmaps']
  verbs: ['*']
- apiGroups: [""]
  resources: ['pods']
  verbs: ['list']
- apiGroups: [tekton.dev]
  resources: [taskruns, pipelineruns]
  verbs: ['*']
- apiGroups: [tekton.dev]
  resources: [pipelines]
  verbs: ['list']
- apiGroups: [kappctrl.k14s.io]
  resources: [apps]
  verbs: ['*']
- apiGroups: [serving.knative.dev]
  resources: ['services']
  verbs: ['*']
- apiGroups: [servicebinding.io]
  resources: ['servicebindings']
  verbs: ['*']
- apiGroups: [services.apps.tanzu.vmware.com]
  resources: ['resourceclaims']
  verbs: ['*']
- apiGroups: [scanning.apps.tanzu.vmware.com]
  resources: ['imagescans', 'sourcescans']
  verbs: ['*']
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: default
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: default
subjects:
  - kind: ServiceAccount
    name: default
EOF
    
}



### EXECUTION STARTS HERE

export TANZU_CLI_NO_INIT=true

#make sure we have a local bin dir in the path
if echo $PATH | grep -q $HOME/bin; then
    echog "found $HOME/bin in PATH"
else
    echor "Didn't find $HOME/bin in PATH.  Please rectify and re-run."
    exit 1
fi

# source the vars file if we have one
if [ -f $VAR_FILE ]; then
    echog "found install-vars file, sourcing"
    source $VAR_FILE
else
    echor "did not find $VAR_FILE, yer on yer own"
fi

#check for commands
#curl
if ! command -v curl &> /dev/null
then
    echor "curl command could not be found"
    sudo apt install -y curl
fi

if command -v docker &> /dev/null && docker version; then
    echog "docker looks good"
else
    echor "problem with docker, either not installed or not non-root"
    exit 1
fi

if ! command -v kubectl &> /dev/null
then
    echog "downloading kubectl"
    install_kubectl
else
    echog "kubectl found"
fi

if ! command -v jq &> /dev/null
then
    echor "jq could not be found"
    sudo apt install -y jq
fi



if command -v tanzu &> /dev/null; then
    if tanzu plugin list | grep apps | grep v0.5.1 &>/dev/null; then
        echog "tanzu and plugins are already installed"
    else
        download_and_install_tanzu_cli_and_plugins
    fi
else
    download_and_install_tanzu_cli_and_plugins
fi

if tanzu secret version | grep v0.11.4 &>/dev/null && tanzu package version | grep v0.11.4 &>/dev/null; then
    echog "Plugins are already patched"
else
    echor "Plugins need to be patched"
    patch_plugins
fi

if [[ $INSTALL_UNMANAGED_CLUSTSER == "true" ]]; then
    echog "install unmanaged"
    install_unmanaged_cluster
else
    echor "don't install unmanaged, expecting a kube cluster to be available"
    if kubectl version &> /dev/null; then
        echog "seems to be a valid kube cluster.  Good Luck!!!"
    else
        echor "No valid kube cluster, exiting"
        exit 1
    fi
fi

if [[ $INSTALL_CLUSTER_ESSENTIALS == "true" ]]; then
    echog "install cluster essentials"
    install_cluster_essentials
else
    echor "don't install cluster essentials.  Expecting SecretExport and App to be available"
    if kubectl get app &> /dev/null && kubectl get secretexport &> /dev/null; then
        echog "seems to be in order.  Good Luck!"
    else
        echor "missing CRDs for apps or secretimport/export"
        exit 1
    fi
fi

if [[ $PATCH_KAPP_CONTROLLER == "true" ]]; then
    echog "patching kapp controller"
    kubectl patch deploy -n $KAPP_CONTROLLER_NS kapp-controller -p '{"spec":{"template":{"spec":{"dnsPolicy": "ClusterFirstWithHostNet"}}}}'
    kubectl delete pod -n $KAPP_CONTROLLER_NS --all
else
    echor "not patching kapp controller - if you're running a TCE unmanaged cluster, this should be ClusterFirstWithHostNet"
    kubectl get deploy -n $KAPP_CONTROLLER_NS kapp-controller -ojsonpath='{.spec.template.spec.dnsPolicy}'
fi

if kubectl get pkgi -n tap-install tap; then
    if kubectl get pkgi tap -n tap-install | grep "Reconcile succeeded"; then
        echog "Tap already installed"
    else
        echor "Tap installed unsuccessfully, exiting"
        exit 1
    fi
else
    echog "tap not found, installing"
    install_tap
fi

echog "setting up dev ns"
install_dev_ns

echog "pulling down pre-pushed workload project"
tanzu apps workload create -n ${DEV_NS} ${WORKLOAD_NAME} --git-repo ${WORKLOAD_GIT_REPO} \
    --git-branch main  --type web --label app.kubernetes.io/part-of=tanzu-java-web-app \
    --annotation autoscaling.knative.dev/minScale=2 --yes


echog "*********"
echog "Visit:  http://tap-gui.${EXTERNAL_FACING_IP}.sslip.io:8080"
echog "*********"

tanzu apps workload tail -n ${DEV_NS} ${WORKLOAD_NAME}